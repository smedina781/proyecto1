var Bicicleta = require('../models/bicicleta');
//GET 
exports.bicicleta_list = function(req, res){
    res.render('bicicletas/index',{bicis: Bicicleta.allBicis});
}
 
//CREATE
exports.bicicleta_create_get = function(req, res){
    res.render('bicicletas/create');
}

exports.bicicleta_create_post = function(req, res){
    var bici = new Bicicleta(req.body.id, req.body.color, req.body.modelo);
    bici.ubicacion=[req.body.lat,req.body.lon];
    Bicicleta.add(bici);
    res.redirect('/bicicletas')

}
//UPDATE
exports.bicicleta_update_get = function(req, res){
    var bici=Bicicleta.findById(req.params.id);
    res.render('bicicletas/update',{bici});
}

exports.bicicleta_update_post = function(req, res){
    var bici=Bicicleta.findById(req.params.id);
    bici.id=req.body.id;
    bici.color=req.body.color;
    bici.modelo=req.body.modelo;
    bici.ubicacion=[req.body.lat,req.body.lon];
    
    res.redirect('/bicicletas')

}

//DELETE
exports.bicicleta_delete_post=function(req,res){
    Bicicleta.removeById(req.params.id);
    res.redirect('/bicicletas')
}
