var Bicicileta=require('../../models/bicicleta');
const Bicicleta = require('../../models/bicicleta');

exports.bicicleta_list=function(req,res){
    res.status(200).json({
        bicicletas:Bicicleta.allBicis
    });
}

exports.bicicletas_create = function(req,res){
    var bici = new Bicicileta(req.body.id, req.body.color,req.body.modelo);
    bici.ubicacion=[req.body.lat,req.body.long];

    Bicicleta.add(bici);

    res.status(200).json({
        bicicleta: bici
    });
}

exports.bicicleta_delete = function(req,res){
    Bicicileta.removeById(req.body.id);
    res.status(204).send();
}

//Update by Id
exports.bicicleta_update=function(req,res){
    var bici = Bicicleta.findById(req.params.id);
    bici.color=req.body.color;
    bici.modelo=req.body.modelo;
    bici.ubicacion=[req.body.lat,req.body.long];

    res.status(200).json({
        bicicleta: bici
    })
   
}