var express = require('express');
var router = express.Router();
var bicicletaController=require('../controllers/bicicleta');
//GET
router.get('/', bicicletaController.bicicleta_list);
//CREATE
router.get('/create', bicicletaController.bicicleta_create_get);
router.post('/create', bicicletaController.bicicleta_create_post);
//UPDATE
router.get('/:id/update', bicicletaController.bicicleta_update_get);
router.post('/:id/update', bicicletaController.bicicleta_update_post);
//DELETE
router.post('/:id/delete',bicicletaController.bicicleta_delete_post);

module.exports=router;