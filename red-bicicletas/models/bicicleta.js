var Bicicleta = function(id, color, modelo, ubicacion){
        this.id=id;
        this.color=color;
        this.modelo=modelo;
        this. ubicacion=ubicacion;
}

Bicicleta.prototype.toString=function(){
    return 'id: '+ this.id + " | color: "+this.color;
}



Bicicleta.allBicis=[];
//Metodo de Agregar
Bicicleta.add = function(aBici){
    Bicicleta.allBicis.push(aBici);
}
//Metodo de Buscar
Bicicleta.findById=function(aBiciId){
    var aBici= Bicicleta.allBicis.find(x=>x.id==aBiciId);
    if(aBici) 
        return aBici;
    else
        throw new Error(`No existe una bicicleta con el id ${aBiciId}`)
}

//Metodo Remover
Bicicleta.removeById=function(aBiciId){
    Bicicleta.findById(aBiciId);
    for(var i=0;i<Bicicleta.allBicis.length;i++){
        if(Bicicleta.allBicis[i].id=aBiciId)
            Bicicleta.allBicis.splice(i,1);
            break;
    }
}




//Crear el objeto y aniadir
var a = new  Bicicleta(1,'rojo','urbana',[14.552894, -90.542668]);
var b = new  Bicicleta(2,'negro','urbana',[14.554182, -90.539843]);

Bicicleta.add(a);
Bicicleta.add(b);

module.exports = Bicicleta;