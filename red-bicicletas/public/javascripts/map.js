var map = L.map('map').setView([14.5527518,-90.5412543], 16);

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
}).addTo(map);

// L.marker([14.552894, -90.542668]).addTo(map)
//    .bindPopup('Bicicleta 1')
//     .openPopup();

// L.marker([14.554182, -90.539843]).addTo(map)
//    .bindPopup('Bicicleta 2')
//     .openPopup();

// L.marker([14.551287, -90.539462]).addTo(map)
//    .bindPopup('Bicicleta 3')
//     .openPopup();

$.ajax({
    dataType:'json',
    url:'api/bicicletas',
    success: function(result){
        console.log(result);
        result.bicicletas.forEach(function(bici){
            L.marker(bici.ubicacion,{title: bici.id}).addTo(map);   
        });
    }
})